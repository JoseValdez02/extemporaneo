  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js";
  import { getAuth, signInWithEmailAndPassword, signOut, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-auth.js";
    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries
  
    const firebaseConfig = {
      apiKey: "AIzaSyCdrNx_TBpENsleT3wgmWz8ez-Z-2spc-Q",
      authDomain: "proyectofinal-fabio.firebaseapp.com",
      databaseURL: "https://proyectofinal-fabio-default-rtdb.firebaseio.com",
      projectId: "proyectofinal-fabio",
      storageBucket: "proyectofinal-fabio.appspot.com",
      messagingSenderId: "841386411710",
      appId: "1:841386411710:web:70239e68777124be9940ef"
    };
  
   // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const auth = getAuth(app);
  
  
  
  
  // Iniciar Sesión
  function login(){
      event.preventDefault();
      let email = document.getElementById('email').value;
      let contra = document.getElementById('contraseña').value;
  
      if(email == "" || contra == ""){
          alert('Complete los campos');
          return;
      }
      const auth = getAuth();
      signInWithEmailAndPassword(auth, email, contra).then((userCredential) => {
      alert('Bienvenido ' + email);
      sessionStorage.setItem('isAuth',"true");
      window.location.href = 'administrador.html';
      
      })
      .catch((error) => {
          alert('Usuario y o contraseña incorrectos')
      });
  
  }
  
  
  
  var btnCerrarSesion = document.getElementById('btnCerrarSesion');
  
  if(btnCerrarSesion){
      btnCerrarSesion.addEventListener('click',  (e)=>{
          signOut(auth).then(() => {
          alert("SESIÓN CERRADA")
          window.location.href="login.html";
          }).catch((error) => {
          });
      });
  }
  
  onAuthStateChanged(auth, async user => {
      console.log(window.location.pathname);
      if (user) {
      } else {
          if (window.location.pathname.includes("administrador")) {
              window.location.href = "/login.html";
          }
      }
  });
  
  var botonLogin = document.getElementById('btnIniciarSesion');
  
  if(botonLogin){
      botonLogin.addEventListener('click', login);
  }