 // Import the functions you need from the SDKs you need
 import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js";
 import { getDatabase, onValue, ref, set, child, get, update, remove} from "https://www.gstatic.com/firebasejs/9.17.2/firebase-database.js";
 import { getStorage,ref as refStorage, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-storage.js";
 
 const firebaseConfig = {
     apiKey: "AIzaSyCdrNx_TBpENsleT3wgmWz8ez-Z-2spc-Q",
     authDomain: "proyectofinal-fabio.firebaseapp.com",
     databaseURL: "https://proyectofinal-fabio-default-rtdb.firebaseio.com",
     projectId: "proyectofinal-fabio",
     storageBucket: "proyectofinal-fabio.appspot.com",
     messagingSenderId: "841386411710",
     appId: "1:841386411710:web:70239e68777124be9940ef"
   };
 
   // Initialize Firebase
 const app = initializeApp(firebaseConfig);
 const db = getDatabase();
 const storage = getStorage();
 
 // Generar Productos
 let productos = document.getElementById('contenido-productos');
 window.addEventListener('DOMContentLoaded',mostrarProductos);
 
 function mostrarProductos(){
     const dbRef = ref(db, "productos");
 
 
     onValue(dbRef,(snapshot) => {
         productos.innerHTML = "";
         snapshot.forEach((childSnapshot) => {
             const childKey = childSnapshot.key;
             const childData = childSnapshot.val();
         
         if(childData.estatus=="1"){
 
             productos.innerHTML +=`
                 <div class='producto'>
                 <img class='img-item' src='${childData.urlImagen}'>
                 <p class='nombre'>${childData.nombre}</p>
                 <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                 <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                 <p class='precio'>\$${childData.precio}</p>
                 <button class='boton-comprar' data-bs-toggle="modal" data-bs-target="#exampleModalToggle">Comprar</button>
                 </div>
             `;
         }
         
         });
     },
     {
         onlyOnce: true,
     }
     );
 }
 
 
 
 
 